% Created on Sun Feb 26 14:01:54 PST 2012

\header {
	title = "We'll Bring the World His Truth" 
	subtitle = "Army of Helaman"
 	composer = "Janice Kapp Perry" 

}
\paper {
 oddFooterMarkup = \markup {"Children's Songbook 172"  } 
 
}
alternateEndingNotes = {
		c2. ^"C" (c2) c4
		<c f >2 ^"F" g'4
		<c, f a>4 ^"G7" (c') <d, f b>4
		<c c'>4 ^"C" (e f
		<g c>2) g4
		<c, f c'>4 ^"F" e d
		<c e c'>^"C"
		
	}
 
mynotes = {
	c4 ^C e f 
	<c g'>2 g'4
	<c, f>4 ^F g'4. a8
	<d, g>2 ^G g4
	<c, e c'>2 ^Am c'4 \break
	<e, b'>4 ^Em g4 g4	
	<c, a'> ^F  (g'4.) f8 
	<c g'>2. ^"G7"
	c4 ^C e f 
	<c g'>2 g'4
	<c, f>4 ^F g'4. a8
	<d, g>2 ^G g4 \break
	<c, e c'>2 ^Am c'4
	<c, e c'>4 ^C g'4 e4 
	d2 ^"G7" c4
	c4 ^C (d8 e f g) ^"C7" 
	a8 ^F (c,8 f4) <c f a>4
	b'8 ^"G7" (d,8 g4) <d g b>
	<g c> ^C g f \break
	<c e>8 ^Am d8 c2
	a'4 ^F a a
	<c, f a> ^Dm f e
	<c d>2 ^"G7" (c4
	d2) ces4 ^"C7" \pageBreak 
	a'8 ^F (c,8 f4) <f a>
	b8 ^"G7" (d,8 g4) <d g b>
	<g c> ^C g f 
	<c e> ^Am d c \break
	<c f>2 ^"Dm7" e4
	d ^"G7" (c) d
	\set Score.repeatCommands = #'((volta "1.2."))
	 c2. ^C
	(c2.) 
	\set Score.repeatCommands = #'((volta "f"))
	\set Score.repeatCommands = #'((volta "3."))
	\alternateEndingNotes
	\set Score.repeatCommands = #'((volta "f"))
	

}




\score { 
	<<
	\new Staff {
		\time 3/4		
		\clef treble
		\relative c' { 	
		 % Type notes here 
		  \mynotes			
		}	
	}
	%verse 1
	\addlyrics {
		We have been 
		born, as
		Ne -- phi of
		old, To good -- ly
		par -- ents who 
		love the 
		Lord.  
		We have been
		taught, and 
		we un -- der -- 
		stand, That
		we must 
		do as the 
		Lord com -- mands
		%chorus
		We are
		as the 
		ar -- my of Hel -- a -- man
		We have been
		taught in our 
		youth 
		And
		we will be the 
		Lord's mis -- sion -- 
		ar -- ies To
		bring the 
		world his
		truth
		%extended third verse ending
		truth.
		To bring the 
		world his
		truth.	
	}
	%verse 2
	\addlyrics{
		We have been
		saved for
		these lat ter
		days To 
		build the king dom in 
		righ teous 
		ways. 
		We ear the 
		words our 
		proph et de 
		clares: Let 
		each who's 
		wor thy go
		forth and share.
		
		
	}
	%verse 3
	\addlyrics {
		We know his
		plan, and 
		we will pre
		pare, In 
		crease our
		knowl edge through 
		study and
		prayer
		Dai ly we'll
		learn un 
		til we are 
		called to 
		take the 
		gos pel to
		all the
		world
		
			
		
	}	
	
	\new TabStaff {
		\set TabStaff.stringTunings = #ukulele-tuning
		\relative c'{
			\mynotes
		}
	}
	\new ChordNames {
		\set Staff.midiInstrument = "acoustic guitar"
		\mynotes
	}

	\new FretBoards {
		\set Staff.stringTunings = #ukulele-tuning
		\relative c'{
			\mynotes
		}
	}

	

>>

  \midi { }
  \layout { }

}
	
	




