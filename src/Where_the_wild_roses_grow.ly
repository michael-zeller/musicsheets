\include "predefined-ukulele-fretboards.ly"


\header {
  title = "Where the wild roses grow"
  composer = "Nick Cave & Kylie Minogue"
}





lyr_a_nick = \lyricmode{

From the first day I saw her, I knew she was the one
She stared in my eyes and smiled
For her lips were the colour of the roses
That grew down the river, all bloody and wild
}

lyr_b_kylie = \lyricmode{

When he knocked on my door and entered the room
My trembling subsided in his sure embrace
He would be my first man, and with a careful hand
He wiped at the tears that ran down my face
}

lyr_c_nick = \lyricmode{

On the second day, I brought her a flower
She was more beautiful than any woman I've seen
I said, "Do you know where the wild roses grow
So sweet and scarlet and free?"


}
lyr_d_kylie = \lyricmode{

On the second day, he came with a single red rose
He said, "Give me your loss and your sorrow"
I nodded my head as I lay on the bed
If I show you the roses, will you follow?
}
lyr_e_kylie = \lyricmode{

On the third day, he took me to the river
He showed me the roses and we kissed
And the last thing I heard was a muttered word
As he knelt above me with a rock in his fist
}
lyr_f_nick = \lyricmode{

On the last day I took her where the wild roses grow
She lay on the bank, the wind light as a thief
And I kissed her goodbye, said, "All beauty must die"
And lent down and planted a rose between her teeth
}

lyr_chorus_end = \lyricmode{
My name was Elisa Day
For my name was Elisa Day
}







% chordoverview

chordoverview = {
\chordmode{
  g:m c:m bes g d f d:7
}
}


\score{
<<
\new ChordNames { 
  \set ChordNames.stringTunings = #ukulele-tuning
  \chordoverview
}
\new FretBoards {
		\set Staff.stringTunings = #ukulele-tuning
		\chordoverview
}
>>
}

% Intro



chords_intro = \chordmode{

  g1.:m c:m g2.:m f g1.:m
}

tabs_intro = \relative c''{
g4\4 d g bes g bes 
c g dis\3 bes' g d 
g\4 d g a\4 c,\3 f 
g\4 d g bes2.
}

\score{

  \header{
    piece = "Intro"
  }
  
<<
	
\new ChordNames { 
    \time 6/4	
    \chords_intro
  }
\new Staff {
		\time 6/4		
		\clef treble
		\tabs_intro
		
	}
\new TabStaff {
		\set TabStaff.stringTunings = #ukulele-tuning
		\time 6/4	
		\tabs_intro
		
	}

>>
}


% Main

chords_chorus = {

\chordmode{  
  g2.:m c:m g:m
  bes bes g 
}
}

lyr_chorus = \lyricmode{
  Th4__ey call me the Wild2. Ro __ se \break
But4 my name was Eli--sa Day
Why they call me it, I do not know
For my name was Elisa Day
}

\score{


<<
  \new ChordNames  \with {
    \override BarLine.bar-extent = #'(-2 . 2)
    \consists "Bar_engraver"
  }{ 
    \time 6/4		
    \partial 2 s4 s4 |
    \chords_chorus
  }
  \new Voice = "chords" {
    %\easyHeadsOn
		\time 6/4		
		\clef treble
    \partial 2 g4 d |
		\chords_chorus
		
	}
  
  \new Lyrics { 
    \time 6/4		
    \lyr_chorus
  }
  
>>


}