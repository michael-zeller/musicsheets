\version "2.22.0"

% LilyBin
\header {
	title = "Hallelujah" 
 	composer = "Leonard Cohen" 
}

parto = \relative c' { 
	c ^C e f 
	<c g'> g'
	<c, f> ^F g' a
}
parta = { \parto\parto }
partb = { \parta\parta }
partc = { \partb\partb }
partd = { \partc\partc }

mychords = \chordmode {
	\time 3/4
	c2. a d:m a b d
}

\score { 
	<<
		\new ChordNames {
				
			
			  
			  \mychords
			
		}
		\new TabStaff {
			\set TabStaff.stringTunings = #ukulele-tuning
			\relative c{
				\mychords
			}
		}
        \new Staff {
            \time 3/4		
            \clef treble
            \relative c' {              
                
            }	
        }
        \addlyrics {
            Now I've heard there was a secret chord
            That David played, and it pleased the Lord
            But you dont really care for music, do you?
            It goes like this, the fourth, the fifth
            The minor falls, the major lifts
            The baffled king composing Hallelujah
			
			Hallelujah, Hallelujah
			Hallelujah, Hallelujah
        }
		
		\addlyrics {
            Your faith was strong but you needed proof
			You saw her bathing on the roof
			Her beauty and the moonlight overthrew her
			She tied you to a kitchen chair
			She broke your throne, and she cut your hair
			And from your lips she drew the Hallelujah
			
			Hallelujah, Hallelujah
			Hallelujah, Hallelujah											
        }
    >>
    \midi { }
    \layout { }
}
